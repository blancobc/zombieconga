﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public float speed;
	private Transform point;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
		point = GameObject.Find("spawnPoint").transform;;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnBecameInvisible()
	{
		if (Camera.main == null) return;

		float yMax = Camera.main.orthographicSize - 0.5f;
		transform.position = new Vector3( point.position.x, 
		                                 Random.Range(-yMax, yMax), 
		                                 0 );
	}

}
