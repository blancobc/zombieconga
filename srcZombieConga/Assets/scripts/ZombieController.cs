﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombieController : MonoBehaviour {

	public float moveSpeed, turnSpeed;
	private Vector3 moveDirection;
	private List<Transform> congaLine = new List<Transform>();
	public int congawin = 15;
	public int lives = 3;
	public AudioClip enemyContactSound;
	public AudioClip catContactSound;




	// Use this for initialization
	void Start () {
		moveDirection = Vector3.right;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 currentPosition = transform.position;

		Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		moveDirection = moveToward - currentPosition;
		moveDirection.z = 0; 

		float distanceToTarget = moveDirection.magnitude;
		if (distanceToTarget > 0)
		{

			// mover hacia el raton
			if ( distanceToTarget > 0.2 )
			{
			moveDirection.Normalize();
			Vector3 target = moveDirection * moveSpeed + currentPosition;
			transform.position = Vector3.Lerp( currentPosition, target, Time.deltaTime);
			}

			// girar
			if ( distanceToTarget > 0.2 )
			{
			float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Slerp( transform.rotation, 
		                                      Quaternion.Euler( 0, 0, targetAngle ), 
			                                  turnSpeed * Time.deltaTime );
			}


		}

		EnforceBounds();

	}

	void OnTriggerEnter2D( Collider2D other )
	{

		if(other.CompareTag("Pickup")) {
			GetComponent<AudioSource>().PlayOneShot(catContactSound);
			Transform followTarget = congaLine.Count == 0 ? transform : congaLine[congaLine.Count-1];
			other.GetComponent<CatController>().addConga (followTarget, moveSpeed, turnSpeed);
			congaLine.Add( other.transform );

			if (congaLine.Count >= congawin)	Application.LoadLevel("win");

		}
		else if (other.CompareTag("Enemy")) {
			GetComponent<AudioSource>().PlayOneShot(enemyContactSound);
			for( int i = 0; i < 2 && congaLine.Count > 0; i++ )
			{
				int lastIdx = congaLine.Count-1;
				Transform cat = congaLine[ lastIdx ];
				congaLine.RemoveAt(lastIdx);
				cat.GetComponent<CatController>().ExitConga();
			}

			lives--;
			if (lives <= 0)	Application.LoadLevel("lose");

		}
	}


	private void EnforceBounds()
	{
		Vector3 newPosition = transform.position; 
		Camera c = Camera.main;
		
		float xDist = c.aspect * c.orthographicSize; 
		float xMax = c.transform.position.x + xDist;
		float xMin = c.transform.position.x - xDist;
		float yDist = c.orthographicSize;


		if ( newPosition.x < xMin || newPosition.x > xMax )
			newPosition.x = Mathf.Clamp( newPosition.x, xMin, xMax );
	
		if (newPosition.y < yDist * -1 || newPosition.y > yDist) 
			newPosition.y = Mathf.Clamp( newPosition.y, yDist * -1, yDist );


		transform.position = newPosition;
	}

}
