﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public float speed = 1f;
	private Vector3 newPosition;
	public Texture2D iconTexture;
	public GUIStyle mystyle;
	public GameObject go;

	// Use this for initialization
	void Start () {
		newPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		newPosition.x = transform.position.x + Time.deltaTime * speed;
		transform.position = newPosition;
	
	}
	
	void OnGUI()
	{
		DisplayInfo();
	}
	
	void DisplayInfo(){
		Rect iconRect = new Rect(10, 10, 32, 32);
		GUI.DrawTexture(iconRect, iconTexture);                         
		
		GUIStyle style = new GUIStyle();
		style.fontSize = 26;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.red;
		Rect labelRect = new Rect(iconRect.xMax + 5, iconRect.y, 60, 32);

		GUI.Label(labelRect, go.GetComponent <ZombieController>().lives.ToString(), style);
	}
}
