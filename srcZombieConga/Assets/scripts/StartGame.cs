﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		Invoke ("loadlevel", 2f);
	}
	
	// Update is called once per frame
	void Update () {
		//if (Input.GetButton("Fire1")) Application.LoadLevel("game");
	}

	public void loadlevel()
	{
		Application.LoadLevel("game");
	}

	/*
	void OnGUI()
	{
		Rect buttonRect = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
		if (GUI.Button(buttonRect, "Start!")) Application.LoadLevel ("game");
	}
	*/
}
