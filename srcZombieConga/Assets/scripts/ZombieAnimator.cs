﻿using UnityEngine;
using System.Collections;

public class ZombieAnimator : MonoBehaviour {

	public Sprite[] sprites;
	public float frames;
	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		sr = (SpriteRenderer) GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		int index = (int)(Time.timeSinceLevelLoad * frames);
		index = index % sprites.Length;
		sr.sprite = sprites[ index ];
	}
}
